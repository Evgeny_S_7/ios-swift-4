var friendNames = [String]()
//var dict = [Int: String]()
var dict: [Int: String] = [:]


var intArray = [1, 5, 80, 3, 5, 45, 3454, 2]
var strArray: [String] = []

// п.4-5
func addAndSortFriendName(name: String){
    friendNames.append(name)
    friendNames = friendNames.sorted(by: {$0.count < $1.count})
}

// п.7
func printDictValueByKey(key: Int) {
    if let index = dict.keys.firstIndex(of: key) {
        print(String(key) + ": " + dict.values[index])
    }
}

// п.8
func printArr(intArr: inout [Int], strArr: inout [String]) {
    if intArr.isEmpty {
        intArr.append(Int.random(in: 0...1000000))
        print(intArr)
    }
    
    if strArr.isEmpty {
        strArr.append("любое значение")
        print(strArr)
    }
}


print(intArray.sorted(by: <))
print(intArray.sorted(by: >))


addAndSortFriendName(name: "Бро")
addAndSortFriendName(name: "Братишка")
addAndSortFriendName(name: "Братик")
print(friendNames)

// заполнение словаря
for friendName in friendNames {
    dict[friendName.count] = friendName
}
print(dict)

printDictValueByKey(key: 3)
printDictValueByKey(key: 7) // нечёткое ТЗ, результат - ничего не выводим)

printArr(intArr: &intArray, strArr: &strArray)

